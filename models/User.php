<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	
	public static function  tableName (){
		return 'user';
	}
	
	public function rules() //Validation
	{
		return
		
		[
			[['username','password','auth_key',], 'string', 'max' => 255],     //type of field
			[['username','password',], 'required'],								//required
			[['username',], 'unique'],										//username type
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) //מחזירה את האובייקט עם כל הפרטים שלו
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException ('Not supported');
		
		return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
