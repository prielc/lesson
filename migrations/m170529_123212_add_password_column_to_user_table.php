<?php

use yii\db\Migration;

/**
 * Handles adding password to table `user`.
 */
class m170529_123212_add_password_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'password', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'password');
    }
}
